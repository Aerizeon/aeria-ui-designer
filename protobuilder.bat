@echo OFF
set iTMP=%CD%
pushd D:\Development\Libraries\ProtoBuf\src\
for /f %%f in ('dir /b %iTMP%\*.proto') do protoc.exe -I=%iTMP% --cpp_out=%iTMP% %iTMP%\%%f
popd