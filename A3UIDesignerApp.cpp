/***************************************************************
 * Name:      A3UIDesignerApp.cpp
 * Purpose:   Code for Application Class
 * Author:     ()
 * Created:   2014-01-18
 * Copyright:  ()
 * License:
 **************************************************************/

#include "A3UIDesignerApp.h"

//(*AppHeaders
#include "A3UIDesignerMain.h"
#include <wx/image.h>
//*)

IMPLEMENT_APP(A3UIDesignerApp);

bool A3UIDesignerApp::OnInit()
{
    //(*AppInitialize
    bool wxsOK = true;
    wxInitAllImageHandlers();
    if ( wxsOK )
    {
    	A3UIDesignerFrame* Frame = new A3UIDesignerFrame(0);
    	Frame->Show();
    	SetTopWindow(Frame);
    }
    //*)
    return wxsOK;

}
