#include "ClassEditor.h"
#include "wx/treebook.h"

//(*InternalHeaders(ClassEditor)
#include <wx/intl.h>
#include <wx/string.h>
//*)

//(*IdInit(ClassEditor)
const long ClassEditor::ID_STATICTEXT1 = wxNewId();
const long ClassEditor::ID_TEXTCTRL1 = wxNewId();
const long ClassEditor::ID_STATICTEXT2 = wxNewId();
const long ClassEditor::ID_TEXTCTRL2 = wxNewId();
const long ClassEditor::ID_SPINCTRL1 = wxNewId();
const long ClassEditor::ID_BUTTON3 = wxNewId();
const long ClassEditor::ID_BUTTON1 = wxNewId();
const long ClassEditor::ID_BUTTON2 = wxNewId();
//*)

BEGIN_EVENT_TABLE(ClassEditor,wxPanel)
	//(*EventTable(ClassEditor)
	//*)
END_EVENT_TABLE()

ClassEditor::ClassEditor(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size)
{
	//(*Initialize(ClassEditor)
	wxFlexGridSizer* FlexGridSizer2;
	wxBoxSizer* BoxSizer1;
	wxStaticBoxSizer* StaticBoxSizer1;
	wxFlexGridSizer* FlexGridSizer1;

	Create(parent, id, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL, _T("id"));
	SetMinSize(wxSize(200,200));
	StaticBoxSizer1 = new wxStaticBoxSizer(wxVERTICAL, this, _("Class Properties"));
	FlexGridSizer2 = new wxFlexGridSizer(2, 1, 0, 0);
	FlexGridSizer2->AddGrowableCol(0);
	FlexGridSizer1 = new wxFlexGridSizer(3, 3, 0, 0);
	FlexGridSizer1->AddGrowableCol(1);
	StaticText1 = new wxStaticText(this, ID_STATICTEXT1, _("Class:"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT1"));
	FlexGridSizer1->Add(StaticText1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	T_ClassName = new wxTextCtrl(this, ID_TEXTCTRL1, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL1"));
	FlexGridSizer1->Add(T_ClassName, 1, wxALL|wxEXPAND|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer1->Add(-1,-1,1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText2 = new wxStaticText(this, ID_STATICTEXT2, _("Label"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT2"));
	FlexGridSizer1->Add(StaticText2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	T_FontFace = new wxTextCtrl(this, ID_TEXTCTRL2, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL2"));
	FlexGridSizer1->Add(T_FontFace, 1, wxALL|wxEXPAND|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	S_FontSize = new wxSpinCtrl(this, ID_SPINCTRL1, _T("12"), wxDefaultPosition, wxSize(46,21), 0, 0, 100, 12, _T("ID_SPINCTRL1"));
	S_FontSize->SetValue(_T("12"));
	FlexGridSizer1->Add(S_FontSize, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer2->Add(FlexGridSizer1, 1, wxALL|wxEXPAND|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 2);
	BoxSizer1 = new wxBoxSizer(wxHORIZONTAL);
	Button2 = new wxButton(this, ID_BUTTON3, _("Delete"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON3"));
	BoxSizer1->Add(Button2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	B_SaveClassInfo = new wxButton(this, ID_BUTTON1, _("Save"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON1"));
	BoxSizer1->Add(B_SaveClassInfo, 0, wxALL|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer2->Add(BoxSizer1, 1, wxALL|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 2);
	StaticBoxSizer1->Add(FlexGridSizer2, 1, wxALL|wxEXPAND|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 0);
	B_AddElement = new wxButton(this, ID_BUTTON2, _("Add Element"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON2"));
	StaticBoxSizer1->Add(B_AddElement, 0, wxALL|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 5);
	SetSizer(StaticBoxSizer1);
	StaticBoxSizer1->Fit(this);
	StaticBoxSizer1->SetSizeHints(this);

	Connect(ID_BUTTON1,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&ClassEditor::OnB_SaveClassInfoClick);
	Connect(ID_BUTTON2,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&ClassEditor::OnB_AddElementClick);
	//*)
}
void ClassEditor::Update()
{
	T_ClassName->SetValue(ThemeClass->type());
	T_FontFace->SetValue(ThemeClass->fontface());
	S_FontSize->SetValue(ThemeClass->fontsize());
	int sel = ((wxTreebook *)this->GetParent())->GetSelection();
	if(ThemeClass->element_size() > 1)
  {
	for(int i =0; i < ThemeClass->element_size(); i++)
	{
		ElementEditor *Editor = new ElementEditor(this->GetParent());
		Editor->ThemeElement = ThemeClass->mutable_element(i);
		((wxTreebook *)this->GetParent())->InsertSubPage(((wxTreebook *)this->GetParent())->GetSelection(),Editor,Editor->ThemeElement->type(),false);
      Editor->Update();
		((wxTreebook *)this->GetParent())->SendSizeEvent();
	}
	((wxTreebook *)this->GetParent())->SetSelection(sel+1);
  }
	((wxTreebook *)this->GetParent())->SendSizeEvent();
}
ClassEditor::~ClassEditor()
{
	//(*Destroy(ClassEditor)
	//*)
}

void ClassEditor::OnB_SaveClassInfoClick(wxCommandEvent& event)
{
    ThemeClass->set_type(T_ClassName->GetValue());
    ThemeClass->set_fontface(T_FontFace->GetValue());
    ThemeClass->set_fontsize(S_FontSize->GetValue());
    ((wxTreebook*)this->GetParent())->SetPageText(((wxTreebook*)this->GetParent())->GetSelection(),ThemeClass->type());
    ((wxTreebook*)this->GetParent())->SendSizeEvent();
}

void ClassEditor::OnB_AddElementClick(wxCommandEvent& event)
{
  ElementEditor *Editor = new ElementEditor(this->GetParent());
  Editor->ThemeElement = ThemeClass->add_element();
	Editor->ThemeElement->set_type("Default");
	Editor->ThemeElement->add_color(0);
	Editor->ThemeElement->add_color(0);
	Editor->ThemeElement->add_color(0);
	Editor->Update();

  ((wxTreebook*)this->GetParent())->InsertSubPage(((wxTreebook*)this->GetParent())->GetSelection(),Editor,"Default",true);
  ((wxTreebook*)this->GetParent())->SendSizeEvent();
}
