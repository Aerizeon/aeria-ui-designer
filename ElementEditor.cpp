#include "ElementEditor.h"
#include "wx/treebook.h"
#include "wx/wx.h"
#include <sstream>

//(*InternalHeaders(ElementEditor)
#include <wx/intl.h>
#include <wx/string.h>
//*)

//(*IdInit(ElementEditor)
const long ElementEditor::ID_STATICTEXT1 = wxNewId();
const long ElementEditor::ID_TEXTCTRL1 = wxNewId();
const long ElementEditor::ID_STATICTEXT2 = wxNewId();
const long ElementEditor::ID_TEXTCTRL2 = wxNewId();
const long ElementEditor::ID_CHECKBOX1 = wxNewId();
const long ElementEditor::ID_PANEL1 = wxNewId();
const long ElementEditor::ID_LISTBOX1 = wxNewId();
const long ElementEditor::ID_STATICTEXT4 = wxNewId();
const long ElementEditor::ID_STATICTEXT3 = wxNewId();
const long ElementEditor::ID_BUTTON2 = wxNewId();
const long ElementEditor::ID_BUTTON1 = wxNewId();
const long ElementEditor::ID_TIMER1 = wxNewId();
//*)

BEGIN_EVENT_TABLE(ElementEditor,wxPanel)
	//(*EventTable(ElementEditor)
	//*)
END_EVENT_TABLE()

ElementEditor::ElementEditor(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size)
{
	//(*Initialize(ElementEditor)
	wxFlexGridSizer* FlexGridSizer3;
	wxFlexGridSizer* FlexGridSizer2;
	wxBoxSizer* BoxSizer1;
	wxStaticBoxSizer* StaticBoxSizer1;
	wxFlexGridSizer* FlexGridSizer1;

	Create(parent, id, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL, _T("id"));
	StaticBoxSizer1 = new wxStaticBoxSizer(wxVERTICAL, this, _("Element Properties"));
	FlexGridSizer3 = new wxFlexGridSizer(5, 2, 0, 0);
	FlexGridSizer3->AddGrowableCol(0);
	FlexGridSizer3->AddGrowableRow(0);
	FlexGridSizer2 = new wxFlexGridSizer(2, 1, 0, 0);
	FlexGridSizer2->AddGrowableCol(0);
	FlexGridSizer2->AddGrowableRow(1);
	FlexGridSizer1 = new wxFlexGridSizer(3, 2, 0, 0);
	FlexGridSizer1->AddGrowableCol(1);
	StaticText1 = new wxStaticText(this, ID_STATICTEXT1, _("Name"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT1"));
	FlexGridSizer1->Add(StaticText1, 1, wxALL|wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
	T_ElementName = new wxTextCtrl(this, ID_TEXTCTRL1, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL1"));
	FlexGridSizer1->Add(T_ElementName, 1, wxALL|wxEXPAND|wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
	StaticText2 = new wxStaticText(this, ID_STATICTEXT2, _("Color:"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT2"));
	FlexGridSizer1->Add(StaticText2, 1, wxALL|wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
	T_ElementColor = new wxTextCtrl(this, ID_TEXTCTRL2, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL2"));
	T_ElementColor->SetMaxLength(8);
	FlexGridSizer1->Add(T_ElementColor, 1, wxALL|wxEXPAND|wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer1->Add(-1,-1,1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	C_ElementMonoPatch = new wxCheckBox(this, ID_CHECKBOX1, _("Single Patch"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_CHECKBOX1"));
	C_ElementMonoPatch->SetValue(false);
	FlexGridSizer1->Add(C_ElementMonoPatch, 1, wxALL|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer2->Add(FlexGridSizer1, 1, wxALL|wxEXPAND|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	P_TexturePanel = new wxPanel(this, ID_PANEL1, wxDefaultPosition, wxSize(200,200), wxSIMPLE_BORDER|wxTAB_TRAVERSAL|wxFULL_REPAINT_ON_RESIZE, _T("ID_PANEL1"));
	FlexGridSizer2->Add(P_TexturePanel, 1, wxALL|wxEXPAND|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 2);
	FlexGridSizer3->Add(FlexGridSizer2, 1, wxALL|wxEXPAND|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 0);
	l_ElementPatches = new wxListBox(this, ID_LISTBOX1, wxDefaultPosition, wxSize(83,271), 0, 0, 0, wxDefaultValidator, _T("ID_LISTBOX1"));
	l_ElementPatches->SetSelection( l_ElementPatches->Append(_("Top Left")) );
	l_ElementPatches->Append(_("Top Center"));
	l_ElementPatches->Append(_("Top Right"));
	l_ElementPatches->Append(_("Center Left"));
	l_ElementPatches->Append(_("Center"));
	l_ElementPatches->Append(_("Center Right"));
	l_ElementPatches->Append(_("Bottom Left"));
	l_ElementPatches->Append(_("Bottom Center"));
	l_ElementPatches->Append(_("Bottom Right"));
	FlexGridSizer3->Add(l_ElementPatches, 1, wxALL|wxEXPAND|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 2);
	StaticBoxSizer1->Add(FlexGridSizer3, 1, wxALL|wxEXPAND|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 2);
	BoxSizer1 = new wxBoxSizer(wxHORIZONTAL);
	T_PosX = new wxStaticText(this, ID_STATICTEXT4, _("X: 0"), wxDefaultPosition, wxSize(45,-1), 0, _T("ID_STATICTEXT4"));
	BoxSizer1->Add(T_PosX, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	T_PosY = new wxStaticText(this, ID_STATICTEXT3, _("Y: 0"), wxDefaultPosition, wxSize(45,-1), 0, _T("ID_STATICTEXT3"));
	BoxSizer1->Add(T_PosY, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	B_Delete = new wxButton(this, ID_BUTTON2, _("Delete"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON2"));
	BoxSizer1->Add(B_Delete, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 2);
	B_Save = new wxButton(this, ID_BUTTON1, _("Save"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON1"));
	BoxSizer1->Add(B_Save, 1, wxALL|wxALIGN_RIGHT|wxALIGN_TOP, 2);
	StaticBoxSizer1->Add(BoxSizer1, 0, wxALL|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 0);
	SetSizer(StaticBoxSizer1);
	static const char *ElementImage_XPM[] = {
	"64 64 45 1",
	"9 c #947070",
	"< c #7B4A4A",
	"e c #A92020",
	"0 c #8D6666",
	"i c #A48989",
	"r c #713C3C",
	"; c #7C4B4B",
	". c #545454",
	": c #6D3636",
	"5 c #8C6464",
	"y c #703A3A",
	"k c #191919",
	"o c #373737",
	"w c #6E3737",
	"2 c #DCDCDC",
	"t c #A48888",
	"u c #713B3B",
	"f c #6C3434",
	"d c #835757",
	"7 c #672D2D",
	"h c #292929",
	"3 c #8B6262",
	", c #815454",
	"& c #652A2A",
	"p c #8E6666",
	"s c #845858",
	"$ c #632727",
	"j c #1B1B1B",
	"4 c #703939",
	"6 c #825555",
	"* c #662B2B",
	"X c #7C2F2F",
	"# c #642828",
	"q c #7B4B4B",
	"1 c #6C3333",
	"- c #835656",
	"+ c #672C2C",
	"_ c None",
	"g c #242424",
	"> c #652929",
	"8 c #7C4C4C",
	"= c #845757",
	"a c #682D2D",
	"O c #632626",
	"@ c #662A2A",
	"_........._._.........__XXXXXXXXXXXXXXXXXXXXX___________________",
	"..oooooooo_o_oooooooo.._OOO++@#OOOOOOO$&*&#OO___________________",
	".ooooooooo_o_ooooooooo._OOO=-;:>OOOOO>:;,<1#O___________________",
	".ooooooooo_o_ooooooooo._OOO22234>OOO>43222<@O___________________",
	".ooooooooo_o_ooooooooo._OOO222254>O>4522226+O___________________",
	".ooooooooo_o_ooooooooo._OOO2222254745222228@O___________________",
	".ooooooooo_o_ooooooooo._OOO9222220q0222223w#O___________________",
	".ooooooooo_o_ooooeoooo._OOOr522222t222225y>OO___________________",
	".ooooooooo_o_ooooeoooo._OOO>y52222222225y>OOO___________________",
	".ooooooooo_o_ooooeoooo._OOOO>y022222220y>OOOO___________________",
	".ooooooooo_o_ooooeoooo._OOOOO7qt22222tq7OOOOO___________________",
	".ooooooooo_o_ooooeoooo._OOOO>y022222220y>OOOO___________________",
	".ooooooooo_o_ooooeoooo._OOO>4522222222254>OOO___________________",
	".ooooooooo_o_ooooeoooo._OOOu522222i2222254>OO___________________",
	".ooooooooo_o_ooooooooo._OOO922222p;p222223w#O___________________",
	".ooooooooo_o_ooooooooo._OOO222225yay5222228@O___________________",
	".ooooooooo_o_ooooooooo._OOO22225y>O>y522226+O___________________",
	".ooooooooo_o_ooooooooo._OOO2223y>OOO>y3222<@O___________________",
	".ooooooooo_o_ooooooooo._OOOsd8w>OOOOO>w86<f#O___________________",
	".ooooooooo_o_ooooooooo._OOO77@#OOOOOOO#@+@#OO___________________",
	".ooooooooo_o_ooooooooo._OOOOOOOOOOOOOOOOOOOOO___________________",
	".ooooooooo_o_ooooooooo._________________________________________",
	".oooooooo._._.oooooooo._________________________________________",
	".ooooooo.._g_..ooooooo._________________________________________",
	"________________________________________________________________",
	".ooooooo.g_h_g.ooooooo._________________________________________",
	"________________________________________________________________",
	".ooooooo.._g_..ooooooo._________________________________________",
	".oooooooo._._.oooooooo._________________________________________",
	".ooooooooo_o_ooooooojo._________________________________________",
	".ooooooooo_o_oooooojjo._________________________________________",
	".ooooooooo_o_ooooojjjo._________________________________________",
	".ooooooooo_o_oooojjjjo._________________________________________",
	".ooooooooo_o_ooojjjjjo._________________________________________",
	".ooooooooo_o_oojjjjjjo._________________________________________",
	"..oooooooo_o_oooooooo.._________________________________________",
	"_........._._.........__________________________________________",
	"________________________________________________________________",
	"_kk_k_kk________________________________________________________",
	"kko_o_okk_______________________________________________________",
	"kog_g_gok_______________________________________________________",
	"________________________________________________________________",
	"kog_g_gok_______________________________________________________",
	"________________________________________________________________",
	"kog_g_gok_______________________________________________________",
	"kko_o_okk_______________________________________________________",
	"_kk_k_kk________________________________________________________",
	"________________________________________________________________",
	"________________________________________________________________",
	"________________________________________________________________",
	"________________________________________________________________",
	"________________________________________________________________",
	"________________________________________________________________",
	"________________________________________________________________",
	"________________________________________________________________",
	"________________________________________________________________",
	"________________________________________________________________",
	"________________________________________________________________",
	"________________________________________________________________",
	"________________________________________________________________",
	"________________________________________________________________",
	"________________________________________________________________",
	"________________________________________________________________",
	"________________________________________________________________"
	};
	ElementImage = new wxImage(ElementImage_XPM);
	ElementImage_BMP = new wxBitmap(ElementImage_XPM);
	Timer1.SetOwner(this, ID_TIMER1);
	Timer1.Start(1, true);
	StaticBoxSizer1->Fit(this);
	StaticBoxSizer1->SetSizeHints(this);

	Connect(ID_CHECKBOX1,wxEVT_COMMAND_CHECKBOX_CLICKED,(wxObjectEventFunction)&ElementEditor::OnC_ElementMonoPatchClick);
	P_TexturePanel->Connect(wxEVT_PAINT,(wxObjectEventFunction)&ElementEditor::OnP_TexturePanelPaint,0,this);
	P_TexturePanel->Connect(wxEVT_ERASE_BACKGROUND,(wxObjectEventFunction)&ElementEditor::OnP_TexturePanelEraseBackground,0,this);
	P_TexturePanel->Connect(wxEVT_LEFT_DOWN,(wxObjectEventFunction)&ElementEditor::OnP_TexturePanelLeftDown,0,this);
	P_TexturePanel->Connect(wxEVT_LEFT_UP,(wxObjectEventFunction)&ElementEditor::OnP_TexturePanelLeftUp,0,this);
	P_TexturePanel->Connect(wxEVT_MOTION,(wxObjectEventFunction)&ElementEditor::OnP_TexturePanelMouseMove,0,this);
	P_TexturePanel->Connect(wxEVT_SIZE,(wxObjectEventFunction)&ElementEditor::OnP_TexturePanelResize,0,this);
	Connect(ID_LISTBOX1,wxEVT_COMMAND_LISTBOX_SELECTED,(wxObjectEventFunction)&ElementEditor::Onl_ElementPatchesSelect);
	Connect(ID_BUTTON1,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&ElementEditor::OnB_SaveClick);
	Connect(ID_TIMER1,wxEVT_TIMER,(wxObjectEventFunction)&ElementEditor::OnTimer1Trigger);
	//*)
}
bool IgnoreInput = true;
void ElementEditor::Update()
{
	T_ElementName->SetValue(ThemeElement->type());
	std::stringstream ColorText;
	if(ThemeElement->color_size() == 4)
		ColorText << std::hex << (ThemeElement->color(0)*255) << (ThemeElement->color(1)*255) << (ThemeElement->color(2)*255) << (ThemeElement->color(3)*255);
	std::cout << "Color:" << ColorText.str() << std::endl;
	T_ElementColor->SetValue(ColorText.str());

	if(ThemeElement->patch_size() == 1)
	{
		C_ElementMonoPatch->SetValue(true);
		l_ElementPatches->Clear();
		l_ElementPatches->Append("Patch");
	}
	else if(ThemeElement->patch_size() == 9)
	{
		C_ElementMonoPatch->SetValue(false);
		l_ElementPatches->Clear();
		l_ElementPatches->Append("Top Left");
		l_ElementPatches->Append("Top Center");
		l_ElementPatches->Append("Top Right");
		l_ElementPatches->Append("Center Left");
		l_ElementPatches->Append("Center");
		l_ElementPatches->Append("Center Right");
		l_ElementPatches->Append("Bottom Left");
		l_ElementPatches->Append("Bottom Center");
		l_ElementPatches->Append("Bottom Right");
	}
	else
	{
		ThemeElement->clear_patch();
		for(int i = 0; i < 9; i++)
			ThemeElement->add_patch();
	}

	CurrentPatch = ThemeElement->mutable_patch(0);
}

ElementEditor::~ElementEditor()
{
	//(*Destroy(ElementEditor)
	//*)
}

void ElementEditor::OnB_SaveClick(wxCommandEvent &event)
{
	((wxTreebook *)this->GetParent())->SetPageText(((wxTreebook *)this->GetParent())->GetSelection(),T_ElementName->GetValue());
	((wxTreebook *)this->GetParent())->SendSizeEvent();
	ThemeElement->set_type(T_ElementName->GetValue());
}

void ElementEditor::OnP_TexturePanelLeftDown(wxMouseEvent &event)
{
	if(IgnoreInput)
		return;
	wxPoint Pos = event.GetPosition();
	CurrentPatch->set_xstart(round(((float)Pos.x/P_TexturePanel->GetSize().x)*ElementImage->GetSize().x)/ElementImage->GetSize().x);
	CurrentPatch->set_xend(round(((float)Pos.x/P_TexturePanel->GetSize().x)*ElementImage->GetSize().x)/ElementImage->GetSize().x);
	CurrentPatch->set_ystart(round(((float)Pos.y/P_TexturePanel->GetSize().y)*ElementImage->GetSize().y)/ElementImage->GetSize().y);
	CurrentPatch->set_yend(round(((float)Pos.y/P_TexturePanel->GetSize().y)*ElementImage->GetSize().y)/ElementImage->GetSize().y);
	P_TexturePanel->Refresh();
}

void ElementEditor::OnP_TexturePanelLeftUp(wxMouseEvent &event)
{
	if(IgnoreInput)
		return;
	wxPoint Pos = event.GetPosition();
	CurrentPatch->set_xend(round(((float)Pos.x/P_TexturePanel->GetSize().x)*ElementImage->GetSize().x)/ElementImage->GetSize().x);
	CurrentPatch->set_yend(round(((float)Pos.y/P_TexturePanel->GetSize().y)*ElementImage->GetSize().y)/ElementImage->GetSize().y);
	P_TexturePanel->Refresh();
}

void ElementEditor::OnP_TexturePanelPaint(wxPaintEvent &event)
{
	wxPaintDC PDC(P_TexturePanel);
	PDC.SetPen(*wxRED_PEN);
	PDC.SetBrush(*wxTRANSPARENT_BRUSH);
	wxSize WinSize = P_TexturePanel->GetSize();
	wxImage Tmp = ElementImage->Scale(WinSize.x,WinSize.y,wxIMAGE_QUALITY_NEAREST);
	PDC.DrawBitmap(wxBitmap(Tmp,32),wxPoint(0,0));
	if(CurrentPatch != NULL && CurrentPatch->has_xstart() && CurrentPatch->has_ystart() && CurrentPatch->has_xend() && CurrentPatch->has_yend())
	{
		PDC.DrawRectangle(wxRect(wxPoint(CurrentPatch->xstart()*P_TexturePanel->GetSize().x, CurrentPatch->ystart()*P_TexturePanel->GetSize().y),
                             wxPoint(CurrentPatch->xend()*P_TexturePanel->GetSize().x, CurrentPatch->yend()*P_TexturePanel->GetSize().y)));
	}
}
void ElementEditor::OnP_TexturePanelMouseMove(wxMouseEvent &event)
{
	if(IgnoreInput)
		return;
	wxPoint Pos = event.GetPosition();
	if(event.ButtonIsDown(wxMOUSE_BTN_LEFT))
	{
		CurrentPatch->set_xend(round(((float)Pos.x/P_TexturePanel->GetSize().x)*ElementImage->GetSize().x)/ElementImage->GetSize().x);
		CurrentPatch->set_yend(round(((float)Pos.y/P_TexturePanel->GetSize().y)*ElementImage->GetSize().y)/ElementImage->GetSize().y);
		P_TexturePanel->Refresh();
	}
	std::stringstream strx,stry;
	strx << "X: " << round(((float)Pos.x/P_TexturePanel->GetSize().x)*ElementImage->GetSize().x);
	stry << "Y: " << round(((float)Pos.y/P_TexturePanel->GetSize().y)*ElementImage->GetSize().y);
	T_PosX->SetLabel(wxString(strx.str()));
	T_PosY->SetLabel(wxString(stry.str()));
}

void ElementEditor::Onl_ElementPatchesSelect(wxCommandEvent &event)
{
	CurrentPatch = ThemeElement->mutable_patch(event.GetInt());
	P_TexturePanel->Refresh();
}

void ElementEditor::OnC_ElementMonoPatchClick(wxCommandEvent &event)
{
	if(wxMessageBox("Changing Element Type destroys all patch data - Continue?","Element Modification",wxYES_NO) == wxNO)
	{
		event.Skip();
		return;
	}
	if(C_ElementMonoPatch->GetValue())
	{
		l_ElementPatches->Clear();
		l_ElementPatches->Append("Patch");
		ThemeElement->clear_patch();
		ThemeElement->add_patch();
	}
	else
	{
		l_ElementPatches->Clear();
		l_ElementPatches->Append("Top Left");
		l_ElementPatches->Append("Top Center");
		l_ElementPatches->Append("Top Right");
		l_ElementPatches->Append("Center Left");
		l_ElementPatches->Append("Center");
		l_ElementPatches->Append("Center Right");
		l_ElementPatches->Append("Bottom Left");
		l_ElementPatches->Append("Bottom Center");
		l_ElementPatches->Append("Bottom Right");
		for(int i = 0; i < 9; i++)
			ThemeElement->add_patch();
	}
	CurrentPatch = ThemeElement->mutable_patch(0);
}

void ElementEditor::OnP_TexturePanelEraseBackground(wxEraseEvent &event)
{
}

void ElementEditor::OnP_TexturePanelResize(wxSizeEvent &event)
{
	wxSize Size = event.GetSize();
	if(Size.GetX() > Size.GetY())
		P_TexturePanel->SetSize(wxDefaultCoord,wxDefaultCoord,Size.GetY(),Size.GetY());
	else if(Size.GetY() > Size.GetX())
		P_TexturePanel->SetSize(wxDefaultCoord,wxDefaultCoord,Size.GetX(),Size.GetX());
}

void ElementEditor::OnTimer1Trigger(wxTimerEvent& event)
{
  IgnoreInput = false;
  std::cout << "Timer Fired" << std::endl;
}
