/***************************************************************
 * Name:      A3UIDesignerMain.h
 * Purpose:   Defines Application Frame
 * Author:     ()
 * Created:   2014-01-18
 * Copyright:  ()
 * License:
 **************************************************************/

#ifndef A3UIDESIGNERMAIN_H
#define A3UIDESIGNERMAIN_H

#include "A3Theme.pb.h"

//(*Headers(A3UIDesignerFrame)
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/menu.h>
#include <wx/textctrl.h>
#include <wx/panel.h>
#include <wx/treebook.h>
#include <wx/filedlg.h>
#include <wx/statbmp.h>
#include <wx/button.h>
#include <wx/frame.h>
//*)

class A3UIDesignerFrame: public wxFrame
{
    public:

        A3UIDesignerFrame(wxWindow* parent,wxWindowID id = -1);
        virtual ~A3UIDesignerFrame();
        A3THeader ThemeBase;

    private:

        //(*Handlers(A3UIDesignerFrame)
        void OnButton1Click(wxCommandEvent& event);
        void OnTreebook1PageChanged(wxNotebookEvent& event);
        void OnTreebook1PageChanged1(wxNotebookEvent& event);
        void OnB_AddClassClick(wxCommandEvent& event);
        void OnTreebook2PageChanged(wxNotebookEvent& event);
        void OnButton1Click1(wxCommandEvent& event);
        void OnButton1Click2(wxCommandEvent& event);
        void OnFileQuitSelected(wxCommandEvent& event);
        void OnFileSaveSelected(wxCommandEvent& event);
        void OnFileOpenSelected(wxCommandEvent& event);
        void OnLeftDown(wxMouseEvent& event);
        //*)

        //(*Identifiers(A3UIDesignerFrame)
        static const long ID_STATICTEXT1;
        static const long ID_TEXTCTRL1;
        static const long ID_STATICTEXT2;
        static const long ID_STATICTEXT3;
        static const long ID_STATICBITMAP1;
        static const long ID_BUTTON2;
        static const long ID_BUTTON1;
        static const long ID_PANEL1;
        static const long ID_TREEBOOK1;
        static const long ID_MENUITEM1;
        static const long ID_MENUITEM3;
        static const long ID_MENUITEM2;
        //*)

        //(*Declarations(A3UIDesignerFrame)
        wxPanel* TreePage1;
        wxButton* B_AddClass;
        wxStaticText* StaticText2;
        wxButton* Button1;
        wxStaticBitmap* StaticBitmap1;
        wxStaticText* StaticText1;
        wxStaticText* StaticText3;
        wxMenu* Menu1;
        wxMenuItem* FileSave;
        wxMenuItem* FileOpen;
        wxMenuItem* FileQuit;
        wxTreebook* Treebook1;
        wxFileDialog* ThemeFileDialog;
        wxTextCtrl* T_ThemeName;
        //*)

        DECLARE_EVENT_TABLE()
};

#endif // A3UIDESIGNERMAIN_H
