#ifndef ELEMENTEDITOR_H
#define ELEMENTEDITOR_H

#include "A3Theme.pb.h"


//(*Headers(ElementEditor)
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
#include <wx/checkbox.h>
#include <wx/listbox.h>
#include <wx/bitmap.h>
#include <wx/panel.h>
#include <wx/button.h>
#include <wx/image.h>
#include <wx/timer.h>
//*)

class ElementEditor: public wxPanel
{
	public:

		ElementEditor(wxWindow* parent,wxWindowID id=wxID_ANY,const wxPoint& pos=wxDefaultPosition,const wxSize& size=wxDefaultSize);
		void Update();
		virtual ~ElementEditor();
		A3TElement  *ThemeElement   = NULL;
		A3TPatch    *CurrentPatch   = NULL;

		//(*Declarations(ElementEditor)
		wxBitmap              *ElementImage_BMP;
		wxTextCtrl* T_ElementColor;
		wxButton* B_Save;
		wxStaticText* T_PosX;
		wxStaticText* StaticText2;
		wxCheckBox* C_ElementMonoPatch;
		wxStaticText* StaticText1;
		wxImage               *ElementImage;
		wxButton* B_Delete;
		wxTextCtrl* T_ElementName;
		wxListBox* l_ElementPatches;
		wxStaticText* T_PosY;
		wxPanel* P_TexturePanel;
		wxTimer Timer1;
		//*)

	protected:

		//(*Identifiers(ElementEditor)
		static const long ID_STATICTEXT1;
		static const long ID_TEXTCTRL1;
		static const long ID_STATICTEXT2;
		static const long ID_TEXTCTRL2;
		static const long ID_CHECKBOX1;
		static const long ID_PANEL1;
		static const long ID_LISTBOX1;
		static const long ID_STATICTEXT4;
		static const long ID_STATICTEXT3;
		static const long ID_BUTTON2;
		static const long ID_BUTTON1;
		static const long ID_TIMER1;
		//*)

	private:

		//(*Handlers(ElementEditor)
		void OnButton1Click(wxCommandEvent& event);
		void OnB_SaveClick(wxCommandEvent& event);
		void OnP_TexturePanelLeftDown(wxMouseEvent& event);
		void OnP_TexturePanelLeftUp(wxMouseEvent& event);
		void OnP_TexturePanelPaint(wxPaintEvent& event);
		void OnP_TexturePanelMouseMove(wxMouseEvent& event);
		void OnCustom1Paint(wxPaintEvent& event);
		void Onl_ElementPatchesSelect(wxCommandEvent& event);
		void OnC_ElementMonoPatchClick(wxCommandEvent& event);
		void OnP_TexturePanelEraseBackground(wxEraseEvent& event);
		void OnP_TexturePanelResize(wxSizeEvent& event);
		void OnTimer1Trigger(wxTimerEvent& event);
		//*)

		DECLARE_EVENT_TABLE()
};

#endif
