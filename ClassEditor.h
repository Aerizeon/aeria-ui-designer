#ifndef CLASSEDITOR_H
#define CLASSEDITOR_H

#include "ElementEditor.h"
#include "A3Theme.pb.h"

//(*Headers(ClassEditor)
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
#include <wx/spinctrl.h>
#include <wx/panel.h>
#include <wx/button.h>
//*)

class ClassEditor: public wxPanel
{
	public:

		ClassEditor(wxWindow* parent,wxWindowID id=wxID_ANY,const wxPoint& pos=wxDefaultPosition,const wxSize& size=wxDefaultSize);
		void Update();
		virtual ~ClassEditor();
		A3TClass *ThemeClass;

		//(*Declarations(ClassEditor)
		wxTextCtrl* T_FontFace;
		wxStaticText* StaticText2;
		wxTextCtrl* T_ClassName;
		wxStaticText* StaticText1;
		wxButton* B_AddElement;
		wxButton* Button2;
		wxButton* B_SaveClassInfo;
		wxSpinCtrl* S_FontSize;
		//*)

	protected:

		//(*Identifiers(ClassEditor)
		static const long ID_STATICTEXT1;
		static const long ID_TEXTCTRL1;
		static const long ID_STATICTEXT2;
		static const long ID_TEXTCTRL2;
		static const long ID_SPINCTRL1;
		static const long ID_BUTTON3;
		static const long ID_BUTTON1;
		static const long ID_BUTTON2;
		//*)

	private:

		//(*Handlers(ClassEditor)
		void OnButton1Click(wxCommandEvent& event);
		void OnB_SaveClassInfoClick(wxCommandEvent& event);
		void OnB_AddElementClick(wxCommandEvent& event);
		//*)

		DECLARE_EVENT_TABLE()
};

#endif
