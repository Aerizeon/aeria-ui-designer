/***************************************************************
 * Name:      A3UIDesignerApp.h
 * Purpose:   Defines Application Class
 * Author:     ()
 * Created:   2014-01-18
 * Copyright:  ()
 * License:
 **************************************************************/

#ifndef A3UIDESIGNERAPP_H
#define A3UIDESIGNERAPP_H

#include <wx/app.h>

class A3UIDesignerApp : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // A3UIDESIGNERAPP_H
