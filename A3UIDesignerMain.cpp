/***************************************************************
 * Name:      A3UIDesignerMain.cpp
 * Purpose:   Code for Application Frame
 * Author:     ()
 * Created:   2014-01-18
 * Copyright:  ()
 * License:
 **************************************************************/

#include "A3UIDesignerMain.h"
#include "ClassEditor.h"
#include <wx/msgdlg.h>
#include <fstream>

//(*InternalHeaders(A3UIDesignerFrame)
#include <wx/bitmap.h>
#include <wx/settings.h>
#include <wx/intl.h>
#include <wx/image.h>
#include <wx/string.h>
//*)

//helper functions
enum wxbuildinfoformat {
    short_f, long_f };

wxString wxbuildinfo(wxbuildinfoformat format)
{
    wxString wxbuild(wxVERSION_STRING);

    if (format == long_f )
    {
#if defined(__WXMSW__)
        wxbuild << _T("-Windows");
#elif defined(__UNIX__)
        wxbuild << _T("-Linux");
#endif

#if wxUSE_UNICODE
        wxbuild << _T("-Unicode build");
#else
        wxbuild << _T("-ANSI build");
#endif // wxUSE_UNICODE
    }

    return wxbuild;
}

//(*IdInit(A3UIDesignerFrame)
const long A3UIDesignerFrame::ID_STATICTEXT1 = wxNewId();
const long A3UIDesignerFrame::ID_TEXTCTRL1 = wxNewId();
const long A3UIDesignerFrame::ID_STATICTEXT2 = wxNewId();
const long A3UIDesignerFrame::ID_STATICTEXT3 = wxNewId();
const long A3UIDesignerFrame::ID_STATICBITMAP1 = wxNewId();
const long A3UIDesignerFrame::ID_BUTTON2 = wxNewId();
const long A3UIDesignerFrame::ID_BUTTON1 = wxNewId();
const long A3UIDesignerFrame::ID_PANEL1 = wxNewId();
const long A3UIDesignerFrame::ID_TREEBOOK1 = wxNewId();
const long A3UIDesignerFrame::ID_MENUITEM1 = wxNewId();
const long A3UIDesignerFrame::ID_MENUITEM3 = wxNewId();
const long A3UIDesignerFrame::ID_MENUITEM2 = wxNewId();
//*)

BEGIN_EVENT_TABLE(A3UIDesignerFrame,wxFrame)
    //(*EventTable(A3UIDesignerFrame)
    //*)
END_EVENT_TABLE()

A3UIDesignerFrame::A3UIDesignerFrame(wxWindow* parent,wxWindowID id)
{
    //(*Initialize(A3UIDesignerFrame)
    wxFlexGridSizer* FlexGridSizer3;
    wxFlexGridSizer* FlexGridSizer2;
    wxBoxSizer* BoxSizer2;
    wxBoxSizer* BoxSizer1;
    wxMenuBar* MenuBar1;
    wxFlexGridSizer* FlexGridSizer1;

    Create(parent, wxID_ANY, _("A3Theme Editor"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_FRAME_STYLE, _T("wxID_ANY"));
    SetClientSize(wxSize(512,395));
    SetMinSize(wxSize(512,400));
    SetMaxSize(wxSize(-1,-1));
    SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_3DLIGHT));
    Treebook1 = new wxTreebook(this, ID_TREEBOOK1, wxPoint(184,120), wxSize(300,209), 0, _T("ID_TREEBOOK1"));
    Treebook1->SetMinSize(wxSize(200,200));
    TreePage1 = new wxPanel(Treebook1, ID_PANEL1, wxPoint(157,84), wxDefaultSize, wxTAB_TRAVERSAL, _T("ID_PANEL1"));
    FlexGridSizer1 = new wxFlexGridSizer(4, 1, 0, 0);
    FlexGridSizer1->AddGrowableCol(0);
    FlexGridSizer1->AddGrowableRow(1);
    FlexGridSizer2 = new wxFlexGridSizer(4, 2, 0, 0);
    FlexGridSizer2->AddGrowableCol(1);
    FlexGridSizer2->AddGrowableRow(3);
    StaticText1 = new wxStaticText(TreePage1, ID_STATICTEXT1, _("Name:"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT1"));
    FlexGridSizer2->Add(StaticText1, 1, wxALL|wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    T_ThemeName = new wxTextCtrl(TreePage1, ID_TEXTCTRL1, _("Text"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL1"));
    FlexGridSizer2->Add(T_ThemeName, 1, wxALL|wxEXPAND|wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    StaticText2 = new wxStaticText(TreePage1, ID_STATICTEXT2, _("Identifier: "), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT2"));
    FlexGridSizer2->Add(StaticText2, 1, wxALL|wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    FlexGridSizer2->Add(-1,-1,1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    StaticText3 = new wxStaticText(TreePage1, ID_STATICTEXT3, _("Version:"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT3"));
    FlexGridSizer2->Add(StaticText3, 1, wxALL|wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
    BoxSizer1 = new wxBoxSizer(wxHORIZONTAL);
    FlexGridSizer2->Add(BoxSizer1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    FlexGridSizer1->Add(FlexGridSizer2, 1, wxALL|wxEXPAND|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 2);
    FlexGridSizer3 = new wxFlexGridSizer(2, 2, 0, 0);
    FlexGridSizer3->AddGrowableCol(1);
    FlexGridSizer3->AddGrowableRow(1);
    StaticBitmap1 = new wxStaticBitmap(TreePage1, ID_STATICBITMAP1, wxBitmap(wxImage(_T("D:\\Development\\Projects\\Aeria\\cache\\textures\\default.png")).Rescale(wxSize(200,200).GetWidth(),wxSize(200,200).GetHeight())), wxDefaultPosition, wxSize(200,200), wxSIMPLE_BORDER, _T("ID_STATICBITMAP1"));
    FlexGridSizer3->Add(StaticBitmap1, 0, wxALL|wxALIGN_LEFT|wxALIGN_TOP, 2);
    FlexGridSizer3->Add(-1,-1,1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    FlexGridSizer3->Add(-1,-1,1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    BoxSizer2 = new wxBoxSizer(wxHORIZONTAL);
    Button1 = new wxButton(TreePage1, ID_BUTTON2, _("Save"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON2"));
    BoxSizer2->Add(Button1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 2);
    B_AddClass = new wxButton(TreePage1, ID_BUTTON1, _("Add Class"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON1"));
    BoxSizer2->Add(B_AddClass, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 2);
    FlexGridSizer3->Add(BoxSizer2, 1, wxALL|wxALIGN_RIGHT|wxALIGN_BOTTOM, 2);
    FlexGridSizer1->Add(FlexGridSizer3, 1, wxALL|wxEXPAND|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 2);
    TreePage1->SetSizer(FlexGridSizer1);
    FlexGridSizer1->Fit(TreePage1);
    FlexGridSizer1->SetSizeHints(TreePage1);
    Treebook1->AddPage(TreePage1, _("Theme                     "), false);
    MenuBar1 = new wxMenuBar();
    Menu1 = new wxMenu();
    FileOpen = new wxMenuItem(Menu1, ID_MENUITEM1, _("&Open"), wxEmptyString, wxITEM_NORMAL);
    Menu1->Append(FileOpen);
    FileSave = new wxMenuItem(Menu1, ID_MENUITEM3, _("&Save"), wxEmptyString, wxITEM_NORMAL);
    Menu1->Append(FileSave);
    Menu1->AppendSeparator();
    FileQuit = new wxMenuItem(Menu1, ID_MENUITEM2, _("&Quit"), wxEmptyString, wxITEM_NORMAL);
    Menu1->Append(FileQuit);
    MenuBar1->Append(Menu1, _("&File"));
    SetMenuBar(MenuBar1);
    ThemeFileDialog = new wxFileDialog(this, _("Select file"), wxEmptyString, _("Theme"), _("*.a3t"), wxFD_SAVE, wxDefaultPosition, wxDefaultSize, _T("wxFileDialog"));
    Center();

    Connect(ID_BUTTON2,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&A3UIDesignerFrame::OnButton1Click2);
    Connect(ID_BUTTON1,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&A3UIDesignerFrame::OnB_AddClassClick);
    Connect(ID_MENUITEM1,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&A3UIDesignerFrame::OnFileOpenSelected);
    Connect(ID_MENUITEM3,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&A3UIDesignerFrame::OnFileSaveSelected);
    Connect(ID_MENUITEM2,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&A3UIDesignerFrame::OnFileQuitSelected);
    //*)
}

A3UIDesignerFrame::~A3UIDesignerFrame()
{
    //(*Destroy(A3UIDesignerFrame)
    //*)
}


void A3UIDesignerFrame::OnB_AddClassClick(wxCommandEvent& event)
{
  ClassEditor *Editor = new ClassEditor(Treebook1);
  Editor->Hide();
  Editor->ThemeClass = ThemeBase.add_class_();
  Editor->ThemeClass->set_type("Window");
  Editor->ThemeClass->set_fontface("Arial");
  Editor->ThemeClass->set_fontsize(12);
  Editor->Update();
  Treebook1->AddSubPage(Editor,_("Window"),true);
  Treebook1->SendSizeEvent();
}


void A3UIDesignerFrame::OnButton1Click2(wxCommandEvent& event)
{
  ThemeFileDialog->SetWindowStyleFlag(wxFD_SAVE);
  ThemeFileDialog->ShowModal();
  std::cout << ThemeFileDialog->GetFilename().c_str()<< std::endl;
  std::ofstream ThemeFile(ThemeFileDialog->GetPath().c_str(),std::ios_base::out|std::ios_base::binary);
  ThemeBase.set_name(T_ThemeName->GetValue());
  ThemeBase.set_version(1);
  ThemeBase.set_identifier(0x30464D41);
  ThemeBase.SerializeToOstream(&ThemeFile);
  ThemeFile.close();
}

void A3UIDesignerFrame::OnFileQuitSelected(wxCommandEvent& event)
{
      Close();
}

void A3UIDesignerFrame::OnFileSaveSelected(wxCommandEvent& event)
{
  ThemeFileDialog->SetWindowStyleFlag(wxFD_SAVE);
  ThemeFileDialog->ShowModal();
  std::ofstream ThemeFile(ThemeFileDialog->GetPath().c_str(),std::ios_base::out|std::ios_base::binary);
  ThemeBase.set_name(T_ThemeName->GetValue());
  ThemeBase.set_version(1);
  ThemeBase.set_identifier(0x30464D41);
  ThemeBase.SerializeToOstream(&ThemeFile);
  ThemeFile.close();
}

void A3UIDesignerFrame::OnFileOpenSelected(wxCommandEvent& event)
{
  ThemeFileDialog->SetWindowStyleFlag(wxFD_OPEN);
	ThemeFileDialog->ShowModal();
	std::cout << ThemeFileDialog->GetFilename() << std::endl;
	std::ifstream ThemeFile(ThemeFileDialog->GetPath().c_str(),std::ios_base::in|std::ios_base::binary);
	ThemeBase.Clear();
	for(int i = Treebook1->GetPageCount()-1; i > 0; i--)
    Treebook1->DeletePage(i);
	ThemeBase.ParseFromIstream(&ThemeFile);
	T_ThemeName->SetValue(ThemeBase.name());

	for(int i = 0; i < ThemeBase.class__size(); i++)
	{
		ClassEditor *Editor = new ClassEditor(Treebook1);
		Editor->ThemeClass = ThemeBase.mutable_class_(i);
		Treebook1->AddSubPage(Editor,_(Editor->ThemeClass->type()),true);
    Editor->Update();
		Treebook1->SendSizeEvent();
	}
}
