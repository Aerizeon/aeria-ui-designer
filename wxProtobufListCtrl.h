#ifndef WXPROTOBUFLISTCTRL_H
#define WXPROTOBUFLISTCTRL_H

#include "wx/ListCtrl.h"

class wxProtobufListCtrl: public wxListCtrl
{
  public:
    wxProtobufListCtrl( wxWindow *parent,
                wxWindowID winid = wxID_ANY,
                const wxPoint& pos = wxDefaultPosition,
                const wxSize& size = wxDefaultSize,
                long style = wxLC_REPORT,
                const wxValidator& validator = wxDefaultValidator,
                const wxString &name = wxListCtrlNameStr);

    virtual ~wxProtobufListCtrl();
  protected:
  private:
};

#endif // WXPROTOBUFLISTCTRL_H
